﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess
{
    public interface ICheckListItemRepository
    {
        IEnumerable<CheckListItem> Get(int checkListItemId);
        int Create(CheckListItem checkListItem);
        void Update(CheckListItem item);
        void Delete(int id);

        void AttachToCheckList(int checkListItemId, int checkListId);
    }
}
