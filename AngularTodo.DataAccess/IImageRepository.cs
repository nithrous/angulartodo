﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess
{
    public interface IImageRepository
    {
        IEnumerable<Image> Get(int checkListId);
        int Create(Image image);
        void Attach(int checkListId, int imageId);
        void Detach(int checkListId, int imageId);
    }
}
