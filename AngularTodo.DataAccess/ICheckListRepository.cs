﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess
{
    public interface ICheckListRepository
    {
        IEnumerable<CheckList> Get();
        CheckList Get(int id);
        int Create(CheckList checkList);
        void Update(CheckList checkList);
        void Delete(int id);
    }
}
