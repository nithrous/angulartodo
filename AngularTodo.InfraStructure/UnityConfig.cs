﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic;
using AngularTodo.DataAccess;
using AngularTodo.DataAccess.EF;
using Microsoft.Practices.Unity;

namespace AngularTodo.Infrastructure
{
    public static class UnityConfig
    {  
        public static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<DbContext, TodoContext>();

            container.RegisterType<ICheckListRepository, CheckListRepository>();
            container.RegisterType<ICheckListItemRepository, CheckListItemRepository>();
            container.RegisterType<IImageRepository, ImageRepository>();

            container.RegisterType<ICheckListService, CheckListService>();
            container.RegisterType<IImageService, ImageService>();

            return container;
        }
    }
}
