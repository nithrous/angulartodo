# AngularTodo #

This is simple todo list application that describes my .NET and AngularJS skills.

## Features ##
* multiple check lists
* card color changing
* image attachment

## Maintanance ##
### Configure AngularTodo.Presentation/web.config appSettings ###
* ImageUploadFolder - any existing folder on your disc
* ImageHostingUrlBase - image path, stay http://*sitename*/Images