﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess.EF
{
    public class CheckListItemRepository : GenericRepository<CheckListItem>, ICheckListItemRepository
    {
        public CheckListItemRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<CheckListItem> Get(int checkListId)
        {
            return Query().Where(x => x.CheckListId == checkListId).ToList();
        }

        public new int Create(CheckListItem checkListItem)
        {
            return base.Create(checkListItem);
        }

        public new void Update(CheckListItem entity)
        {
            var dbEntity = Query().First(x => x.Id == entity.Id);
            dbEntity.IsChecked = entity.IsChecked;
            dbEntity.Text = entity.Text;
            Context.SaveChanges();
        }

        public new void Delete(int id)
        {
            base.Delete(id);
        }

        public void AttachToCheckList(int checkListItemId, int checkListId)
        {
            var item = Query().First(x => x.Id == checkListItemId);
            if (item == null) throw new ArgumentNullException(nameof(item));
            item.CheckListId = checkListId;
            Update(item);
        }
    }
}
