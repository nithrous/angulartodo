﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess.EF
{
    public abstract class GenericRepository<TEntity> where TEntity : EntityBase
    {
        protected readonly DbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        protected GenericRepository(DbContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            Context = context;

            DbSet = Context.Set<TEntity>();
        }

        protected IQueryable<TEntity> Query()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        protected int Create(TEntity entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
            return entity.Id;
        }

        protected void Delete(int id)
        {
            var entity = DbSet.Find(id);
            entity.IsDeleted = true;
            Context.SaveChanges();
        }

        protected void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
