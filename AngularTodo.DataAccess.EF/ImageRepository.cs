﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess.EF
{
    public class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        public ImageRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Image> Get(int checkListId)
        {
            return Query().Where(x => x.CheckListId == checkListId).ToList();
        }

        public new int Create(Image image)
        {
            return base.Create(image);
        }

        public void Attach(int checkListId, int imageId)
        {
            var image = Query().First(x => x.Id == imageId);
            image.CheckListId = checkListId;
            Update(image);
        }

        public void Detach(int checkListId, int imageId)
        {
            var image = Query().First(x => x.Id == imageId && x.CheckListId == checkListId);
            image.CheckListId = null;
            Update(image);
        }
    }
}
