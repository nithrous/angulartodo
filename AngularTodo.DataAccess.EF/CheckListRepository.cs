﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.DataAccess.EF
{
    public class CheckListRepository : GenericRepository<CheckList>, ICheckListRepository
    {
        public CheckListRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<CheckList> Get()
        {
            return Query().ToList();
        }

        public CheckList Get(int id)
        {
            return Query().First(x => x.Id == id);
        }

        public new int Create(CheckList checkList)
        {
            return base.Create(checkList);
        }

        public new void Update(CheckList checkList)
        {
            base.Update(checkList);
        }

        public new void Delete(int id)
        {
            base.Delete(id);
        }
    }
}
