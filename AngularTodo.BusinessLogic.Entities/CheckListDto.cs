﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.BusinessLogic.Dto
{
    public class CheckListDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Color { get; set; }

        public IEnumerable<CheckListItemDto> Items { get; set; } 
        public IEnumerable<ImageDto> Images { get; set; } 
    }
}
