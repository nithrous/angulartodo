﻿using System;

namespace AngularTodo.BusinessLogic.Dto
{
    public class CheckListItemDto
    {
        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string Text { get; set; }
    }
}
