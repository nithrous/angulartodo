﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.BusinessLogic.Dto.Mappers
{
    public static class CheckListMapper
    {
        public static CheckList ToPoco(this CheckListDto dto)
        {
            return new CheckList
            {
                Id = dto.Id,
                Title = dto.Title,
                Color = dto.Color
            };
        }

        public static CheckListDto ToDto(this CheckList poco)
        {
            return new CheckListDto
            {
                Id = poco.Id,
                Title = poco.Title,
                Color = poco.Color
            };
        }
    }
}
