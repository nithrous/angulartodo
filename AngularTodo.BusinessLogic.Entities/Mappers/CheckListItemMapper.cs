﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.DataAccess.Entities;

namespace AngularTodo.BusinessLogic.Dto.Mappers
{
    public static class CheckListItemMapper
    {
        public static CheckListItemDto ToDto(this CheckListItem poco)
        {
            return new CheckListItemDto
            {
                Id = poco.Id,
                IsChecked = poco.IsChecked,
                Text = poco.Text
            };
        }

        public static CheckListItem ToPoco(this CheckListItemDto dto)
        {
            return new CheckListItem
            {
                Id = dto.Id,
                IsChecked = dto.IsChecked,
                Text = dto.Text
            };
        }

        public static CheckListItem ToPoco(this CheckListItemDto dto, int checkListId)
        {
            return new CheckListItem
            {
                Id = dto.Id,
                IsChecked = dto.IsChecked,
                Text = dto.Text,
                CheckListId = checkListId
            };
        }
    }
}
