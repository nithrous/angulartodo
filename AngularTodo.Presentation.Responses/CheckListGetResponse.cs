﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Responses
{
    public class CheckListGetResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Color { get; set; }
        public IEnumerable<CheckListItemResponse> Items { get; set; }
        public ImageResponse Image { get; set; }
    }
}
