﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Responses
{
    public class CheckListItemPostResponse
    {
        public int Id { get; set; }
    }
}
