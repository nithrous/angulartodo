﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.Presentation.Responses.Mappers
{
    public static class CheckListMapper
    {
        public static CheckListGetResponse ToGetResponse(this CheckListDto dto)
        {
            return new CheckListGetResponse
            {
                Id = dto.Id,
                Color = dto.Color,
                Title = dto.Title,
                Items = dto.Items.Select(x => x.ToResponse()),
                Image = dto.Images.Select(x => x.ToResponse()).FirstOrDefault() // hack: we need only one image in front end
            };
        }
    }
}
