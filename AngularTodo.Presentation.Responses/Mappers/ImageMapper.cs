﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.Presentation.Responses.Mappers
{
    public static class ImageMapper
    {
        public static ImageResponse ToResponse(this ImageDto dto)
        {
            return new ImageResponse
            {
                Id = dto.Id,
                Url = dto.Url
            };
        }
    }
}
