﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.Presentation.Responses.Mappers
{
    public static class CheckListItemMapper
    {
        public static CheckListItemResponse ToResponse(this CheckListItemDto dto)
        {
            return new CheckListItemResponse
            {
                Id = dto.Id,
                IsChecked = dto.IsChecked,
                Text = dto.Text
            };
        }
    }
}
