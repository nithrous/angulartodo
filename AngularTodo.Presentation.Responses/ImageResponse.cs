﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Responses
{
    public class ImageResponse
    {
        public int Id { get; set; }
        public string Url { get; set; }
    }
}
