﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Responses
{
    public class CheckListItemResponse
    {
        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string Text { get; set; }
    }
}
