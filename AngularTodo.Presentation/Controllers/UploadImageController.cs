﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularTodo.BusinessLogic;
using AngularTodo.Presentation.Requests;
using AngularTodo.Presentation.Responses;
using AngularTodo.Presentation.Responses.Mappers;

namespace AngularTodo.Presentation.Controllers
{
    public class UploadImageController : ApiController
    {
        private readonly IImageService _imageService;

        public UploadImageController(IImageService imageService)
        {
            if (imageService == null) throw new ArgumentNullException(nameof(imageService));
            _imageService = imageService;
        }

        public ImageResponse Post([FromBody] ImageUploadRequest request)
        {
            return _imageService.UploadImage(request.Base64String, request.Ext).ToResponse();
        }
    }
}
