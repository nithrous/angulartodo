﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularTodo.BusinessLogic;
using AngularTodo.Presentation.Requests;

namespace AngularTodo.Presentation.Controllers
{
    public class AttachImageController : ApiController
    {
        private readonly IImageService _imageService;

        public AttachImageController(IImageService imageService)
        {
            if (imageService == null) throw new ArgumentNullException(nameof(imageService));
            _imageService = imageService;
        }

        public void Post([FromBody]ImageAttachRequest request)
        {
            _imageService.AttachImage(request.CheckListId, request.ImageId);
        }

        public void Delete([FromUri] ImageDetachRequest request)
        {
            _imageService.DetachImage(request.CheckListId, request.ImageId);
        }
    }
}
