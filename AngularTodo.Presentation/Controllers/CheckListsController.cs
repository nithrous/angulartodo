﻿using AngularTodo.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularTodo.Presentation.Requests;
using AngularTodo.Presentation.Requests.Mappers;
using AngularTodo.Presentation.Responses;
using AngularTodo.Presentation.Responses.Mappers;

namespace AngularTodo.Presentation.Controllers
{
    public class CheckListsController : ApiController
    {
        private readonly ICheckListService _checkListService;

        public CheckListsController(ICheckListService checkListService)
        {
            if (checkListService == null) throw new ArgumentNullException("checkListService");
            _checkListService = checkListService;
        }

        public IEnumerable<CheckListGetResponse> Get()
        {
            return _checkListService.GetAllCheckLists()
                .Select(x => x.ToGetResponse());
        }

        public CheckListPostResponse Post([FromBody]CheckListPostRequest request)
        {
            int id = _checkListService.CreateCheckList(request.ToDto());
            return new CheckListPostResponse { Id = id };
        }

        public void Put([FromBody]CheckListPutRequest request)
        {
            _checkListService.UpdateCheckList(request.ToDto());
        }

        public void Delete([FromUri]CheckListDeleteRequest request)
        {
            _checkListService.DeleteCheckList(request.Id);
        }
    }
}
