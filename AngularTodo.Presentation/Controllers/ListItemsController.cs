﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularTodo.BusinessLogic;
using AngularTodo.Presentation.Requests;
using AngularTodo.Presentation.Requests.Mappers;
using AngularTodo.Presentation.Responses;

namespace AngularTodo.Presentation.Controllers
{
    public class ListItemsController : ApiController
    {
        private readonly ICheckListService _checkListService;

        public ListItemsController(ICheckListService checkListService)
        {
            if (checkListService == null) throw new ArgumentNullException("checkListService");
            _checkListService = checkListService;
        }

        public CheckListItemPostResponse Post([FromBody] CheckListItemPostRequest request)
        {
            int id = _checkListService.AttachItem(request.CheckListId, request.ToDto());
            return new CheckListItemPostResponse {Id = id};
        }

        public void Put([FromBody] CheckListItemPutRequest request)
        {
            _checkListService.UpdateItem(request.ToDto());
        }

        public void Delete([FromUri]CheckListItemDeleteRequest request)
        {
            _checkListService.DetachItem(request.Id);
        }
    }
}
