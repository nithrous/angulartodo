﻿(function() {
    angular
        .module("angularTodo.common")
        .directive("atdFocusOn", focusOnShow);

    function focusOnShow() {
        return {
            restrict: "A",
            link: link
        }
    }

    function link(scope, element, attrs) {
        scope.$on(attrs.atdFocusOn, function () {
            setTimeout(function() {
                element[0].focus();
            }, 0);
        });
    }
})()