﻿(function () {
    angular.module("angularTodo", [
        "ngResource",
        "angularTodo.checklists",
        "angularTodo.common"
    ]);
})()