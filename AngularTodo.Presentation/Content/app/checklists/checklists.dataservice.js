﻿(function() {

    angular
        .module("angularTodo.checklists")
        .factory("checkListsDataService", checkListsDataService);

    checkListsDataService.$inject = ["$resource"];

    function checkListsDataService($resource) {
        
        var service = {
            getAllCheckLists: getAllCheckLists,
            createCheckList: createCheckList,
            updateCheckList: updateCheckList,
            deleteCheckList: deleteCheckList,

            attachListItem: attachListItem,
            detachListItem: detachListItem,
            updateListItem: updateListItem,

            uploadImage: uploadImage,
            attachImage: attachImage,
            detachImage: detachImage
        }

        var checkLists = $resource("/api/checklists", null, { "update" : { method: "PUT" } });
        var listItems = $resource("/api/listitems", null, { "update": { method: "PUT" } });
        var uploadImageResource = $resource("/api/uploadimage");
        var attachImageResource = $resource("/api/attachimage");

        return service;

        function getAllCheckLists() {
            return checkLists.query({}).$promise;
        }

        function createCheckList(checkList) {
            return checkLists.save(checkList).$promise;
        }

        function updateCheckList(checkList) {
            return checkLists.update(checkList).$promise;
        }

        function deleteCheckList(id) {
            return checkLists.delete({ id: id }).$promise;
        }

        function attachListItem(checkListId, text) {
            return listItems.save({
                checkListId: checkListId,
                text: text
            }).$promise;
        }

        function updateListItem(listItem) {
            return listItems.update({
                listItemId: listItem.id,
                isChecked: listItem.isChecked,
                text : listItem.text
            }).$promise;
        }

        function detachListItem(listItemId) {
            return listItems.delete({
                id: listItemId
            }).$promise;
        }

        function uploadImage(base64Str, ext) {
            return uploadImageResource.save({base64String: base64Str, ext: ext}).$promise;
        }

        function attachImage(checkListId, imageId) {
            return attachImageResource.save({
                checkListId: checkListId,
                imageId: imageId
            }).$promise;
        }

        function detachImage(checkListId, imageId) {
            return attachImageResource.delete({
                checkListId: checkListId,
                imageId: imageId
            }).$promise;
        }
    }

})()