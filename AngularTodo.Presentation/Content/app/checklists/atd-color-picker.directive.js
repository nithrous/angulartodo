﻿(function() {
    angular
        .module("angularTodo.checklists")
        .directive("atdColorPicker", atdColorPicker);

    function atdColorPicker() {

        return {
            restrict: "A",
            link: link
        }
    }

    function link(scope, element, attrs) {

        element.bind("click", function() {
            var pickerElement = angular.element("#color-picker");
            pickerElement.css("display", "block");

            var executorElementTop = element.prop("offsetTop");
            var executorElementLeft = element.prop("offsetLeft");

            pickerElement.css("top", executorElementTop - pickerElement[0].offsetHeight);
            pickerElement.css("left", executorElementLeft);

            pickerElement.bind("click", function (event) {
                var target = angular.element(event.target);
                if (target.hasClass("color")) {
                    pickerElement.css("display", "none");
                }
            });
        });

    }
})()