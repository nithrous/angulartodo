﻿(function() {

    angular
        .module("angularTodo.checklists")
        .controller("CheckListsController", CheckListsController);

    CheckListsController.$inject = ["$scope", "checkListsDataService"];

    function CheckListsController(scope, dataservice) {

        var vm = this;

        vm.newCheckList = {};
        vm.checkLists = [];
        vm.colors = [];
        vm.selectedCheckList = null;

        vm.getNewEmptyCheckList = getNewEmptyCheckList;
        vm.startNewCheckListEdition = startNewCheckListEdition;
        vm.cancelNewCheckListEdition = cancelNewCheckListEdition;
        vm.submitNewCheckListCreation = submitNewCheckListCreation;
        vm.updateCheckList = updateCheckList;
        vm.updateListItem = updateListItem;
        vm.addListItem = addListItem;
        vm.removeListItem = removeListItem;
        vm.deleteCheckList = deleteCheckList;
        vm.startStoredCheckListEdition = startStoredCheckListEdition;
        vm.finishStoredCheckListEdition = finishStoredCheckListEdition;
        vm.updateColor = updateColor;
        vm.attachImage = attachImage;
        vm.detachImage = detachImage;

        (function init() {
            vm.newCheckList = getNewEmptyCheckList();
            loadStoredCheckLists();
            vm.colors = getColorIds();
        })();

        function getColorIds() {
            return [0, 1, 2, 3];
        }

        function loadStoredCheckLists() {
            dataservice.getAllCheckLists().then(function(response) {

                var checkLists = [];

                for (var i = 0; i < response.length; i++) {
                    var checkList = {
                        id: response[i].id,
                        title: response[i].title,
                        color: response[i].color,
                        mode: "display",
                        image: response[i].image,
                        items: response[i].items
                    }

                    checkLists.push(checkList);
                }

                vm.checkLists = checkLists;
            });
        }

        function getNewEmptyCheckList() {
            return {
                id: 0,
                title: "",
                color: 0,
                items: [
                    getNewEmptyListItem()
                ],
                image: null,
                mode: "display"
            };
        }

        function getNewEmptyListItem() {
            return {
                id: 0,
                isChecked: false,
                text: ""
            };
        }

        function deleteEmptyListItems(checkList, onServer) {
            for (var i = 0; i < checkList.items.length; i++) {

                var item = checkList.items[i];

                if (item.text === "") {

                    checkList.items.splice(i, 1);
                    i--;

                    if (onServer === true) {
                        dataservice.detachListItem(item.id);
                    }
                }
            }
        }

        function startNewCheckListEdition() {
            highlightCheckList(vm.newCheckList);
            scope.$broadcast("clCreationStarted");

            dataservice.createCheckList(vm.newCheckList).then(function(response) {
                vm.newCheckList.id = response.id;
                dataservice.attachListItem(vm.newCheckList.id, vm.newCheckList.items[0].text).then(function (response) {
                    vm.newCheckList.items[0].id = response.id;
                });
            });
        }

        function cancelNewCheckListEdition() {
            dataservice.deleteCheckList(vm.newCheckList.id);
            vm.newCheckList = getNewEmptyCheckList();
        }

        function submitNewCheckListCreation() {
            deleteEmptyListItems(vm.newCheckList, true);
            vm.newCheckList.mode = "display";

            if (isCheckListEmpty(vm.newCheckList)) {
                dataservice.deleteCheckList(vm.newCheckList.id);
            } else {
                vm.checkLists.unshift(vm.newCheckList);
            }

            vm.newCheckList = getNewEmptyCheckList();
        }

        function updateCheckList(checkList) {
            dataservice.updateCheckList(checkList);
        }

        function updateListItem(listItem) {
            dataservice.updateListItem(listItem);
        }

        function addListItem(checkList) {
            var item = getNewEmptyListItem();
            checkList.items.push(item);

            dataservice.attachListItem(checkList.id, "").then(function(response) {
                item.id = response.id;
            });
        }

        function removeListItem(checkList, item) {
            var index = checkList.items.indexOf(item);
            checkList.items.splice(index, 1);
            dataservice.detachListItem(item.id);
        }

        function deleteCheckList(checkList) {
            dataservice.deleteCheckList(checkList.id);
            var index = vm.checkLists.indexOf(checkList);
            vm.checkLists.splice(index, 1);
        }

        function startStoredCheckListEdition(checkList) {
            highlightCheckList(checkList);
        }

        function finishStoredCheckListEdition(checkList) {
            checkList.mode = "display";
            deleteEmptyListItems(checkList, true);
        }

        function highlightCheckList(checkList) {
            checkList.mode = "edit";
            vm.selectedCheckList = checkList;

            if (vm.newCheckList !== checkList && vm.newCheckList !== undefined) {
                vm.newCheckList.mode = "display";

                if (vm.newCheckList.mode === "edit") {
                    cancelNewCheckListEdition();
                }
            }

            for (var i = 0; i < vm.checkLists.length; i++) {
                if (vm.checkLists[i] !== checkList) {
                    vm.checkLists[i].mode = "display";
                }
            }
        }

        function updateColor(color) {
            vm.selectedCheckList.color = color;
            updateCheckList(vm.selectedCheckList);
        }

        function attachImage(file, base64Obj) {
            var ext = file.name.split('.').pop();

            dataservice.uploadImage(base64Obj.base64, ext).then(function(response) {
                dataservice.attachImage(vm.selectedCheckList.id, response.id);
                vm.selectedCheckList.image = response;
            });
        }

        function detachImage(checkList) {
            dataservice.detachImage(checkList.id, checkList.image.id).then(function() {
                checkList.image = null;
            });
        }

        function isCheckListEmpty(checkList) {
            if (
                    checkList.items.length === 0
                    && (checkList.title === null || vm.newCheckList.title.match(/^ *$/) !== null)
                    && checkList.image == null) {
                return true;
            }

            return false;
        }
    }
})()