﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Requests
{
    public class CheckListPostRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public int Color { get; set; }
    }
}
