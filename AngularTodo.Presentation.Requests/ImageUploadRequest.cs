﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Requests
{
    public class ImageUploadRequest
    {
        [Required]
        public string Base64String { get; set; }

        [Required]
        public string Ext { get; set; }
    }
}
