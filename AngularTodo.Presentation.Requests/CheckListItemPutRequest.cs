﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Requests
{
    public class CheckListItemPutRequest
    {
        [Required]
        public int ListItemId { get; set; }

        [Required]
        public bool IsChecked { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
