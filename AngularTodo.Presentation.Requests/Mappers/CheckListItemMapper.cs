﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.Presentation.Requests.Mappers
{
    public static class CheckListItemMapper
    {
        public static CheckListItemDto ToDto(this CheckListItemPostRequest request)
        {
            return new CheckListItemDto
            {
                Text = request.Text
            };
        }

        public static CheckListItemDto ToDto(this CheckListItemPutRequest request)
        {
            return new CheckListItemDto
            {
                Id = request.ListItemId,
                IsChecked = request.IsChecked,
                Text = request.Text
            };
        }
    }
}
