﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.Presentation.Requests.Mappers
{
    public static class CheckListMapper
    {
        public static CheckListDto ToDto(this CheckListPostRequest request)
        {
            return new CheckListDto
            {
                Title = request.Title,
                Color = request.Color
            };
        }

        public static CheckListDto ToDto(this CheckListPutRequest request)
        {
            return new CheckListDto
            {
                Id = request.Id,
                Title = request.Title,
                Color = request.Color
            };
        }
    }
}
