﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTodo.Presentation.Requests
{
    public class CheckListItemDeleteRequest
    {
        [Required]
        public int Id { get; set; }
    }
}
