﻿using System.Collections.Generic;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.BusinessLogic
{
    public interface IImageService
    {
        IEnumerable<ImageDto> Get(int checkListId);
        ImageDto UploadImage(string base64String, string ext);
        void AttachImage(int checkListId, int imageId);
        void DetachImage(int checkListId, int imageId);
    }
}
