﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;

namespace AngularTodo.BusinessLogic
{
    public interface ICheckListService
    {
        IEnumerable<CheckListDto> GetAllCheckLists();
        int CreateCheckList(CheckListDto dto);
        void UpdateCheckList(CheckListDto dto);
        void DeleteCheckList(int id);

        int AttachItem(int checkListId, CheckListItemDto itemDto);
        void UpdateItem(CheckListItemDto itemDto);
        void DetachItem(int checkListItemId);
    }
}
