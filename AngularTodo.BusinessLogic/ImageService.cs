﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using AngularTodo.BusinessLogic.Dto;
using AngularTodo.DataAccess;
using AngularTodo.DataAccess.Entities;
using Flurl;

namespace AngularTodo.BusinessLogic
{
    public class ImageService : IImageService
    {
        private readonly string _imageFolder = ConfigurationManager.AppSettings["ImageUploadFolder"];
        private readonly string _imageHostingUrlBase = ConfigurationManager.AppSettings["ImageHostingUrlBase"];

        private readonly IImageRepository _imageRepository;

        public ImageService(IImageRepository imageRepository)
        {
            if (imageRepository == null) throw new ArgumentNullException("imageRepository");
            _imageRepository = imageRepository;
        }

        public IEnumerable<ImageDto> Get(int checkListId)
        {
            return _imageRepository.Get(checkListId)
                .Select(x => new ImageDto
                {
                    Id = x.Id,
                    Url = GetAbsoluteUrl(x.RelativeLocation)
                });
        }

        public void AttachImage(int checkListId, int imageId)
        {
            _imageRepository.Attach(checkListId, imageId);
        }

        public void DetachImage(int checkListId, int imageId)
        {
            _imageRepository.Detach(checkListId, imageId);
        }

        public ImageDto UploadImage(string base64String, string ext)
        {
            string fileName = GenerateFileName(ext);
            string path = Path.Combine(_imageFolder, fileName);
            var bytes = Convert.FromBase64String(base64String);

            if (!Directory.Exists(_imageFolder))
                throw new DirectoryNotFoundException("Image folder is missing");

            File.WriteAllBytes(path, bytes);

            int id = _imageRepository.Create(new Image()
            {
                RelativeLocation = fileName
            });

            return new ImageDto
            {
                Id = id,
                Url = GetAbsoluteUrl(fileName)
            };
        }

        private string GetAbsoluteUrl(string relativeLocation)
        {
            return Url.Combine(_imageHostingUrlBase, relativeLocation);
        }

        private string GenerateFileName(string ext)
        {
            return Guid.NewGuid().ToString() + "." + ext;
        }
    }
}
