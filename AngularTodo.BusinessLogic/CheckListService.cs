﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTodo.BusinessLogic.Dto;
using AngularTodo.BusinessLogic.Dto.Mappers;
using AngularTodo.DataAccess;

namespace AngularTodo.BusinessLogic
{
    public class CheckListService : ICheckListService
    {
        private readonly ICheckListRepository _checkListRepository;
        private readonly ICheckListItemRepository _checkListItemRepository;
        private readonly IImageService _imageService;

        public CheckListService(ICheckListRepository checkListRepository, ICheckListItemRepository checkListItemRepository, IImageService imageService)
        {
            if (checkListRepository == null) throw new ArgumentNullException("checkListRepository");
            if (checkListItemRepository == null) throw new ArgumentNullException("checkListItemRepository");
            if (imageService == null) throw new ArgumentNullException("imageService");
            _checkListRepository = checkListRepository;
            _checkListItemRepository = checkListItemRepository;
            _imageService = imageService;
        }

        public IEnumerable<CheckListDto> GetAllCheckLists()
        {
            var checkLists = _checkListRepository.Get()
                .Select(x => x.ToDto())
                .OrderByDescending(x => x.Id)
                .ToList();
            foreach (var checkList in checkLists)
            {
                checkList.Items = _checkListItemRepository.Get(checkList.Id).Select(x => x.ToDto());
                checkList.Images = _imageService.Get(checkList.Id);
            }

            return checkLists;
        }

        public int CreateCheckList(CheckListDto dto)
        {
            return _checkListRepository.Create(dto.ToPoco());
        }

        public void UpdateCheckList(CheckListDto dto)
        {
            _checkListRepository.Update(dto.ToPoco());
        }

        public void DeleteCheckList(int id)
        {
            _checkListRepository.Delete(id);
        }

        public int AttachItem(int checkListId, CheckListItemDto itemDto)
        {
            int itemId = _checkListItemRepository.Create(itemDto.ToPoco(checkListId));
            _checkListItemRepository.AttachToCheckList(itemId, checkListId);

            return itemId;
        }

        public void UpdateItem(CheckListItemDto itemDto)
        {
            _checkListItemRepository.Update(itemDto.ToPoco());
        }

        public void DetachItem(int checkListItemId)
        {
            _checkListItemRepository.Delete(checkListItemId);
        }
    }
}
