﻿using System.ComponentModel.DataAnnotations;

namespace AngularTodo.DataAccess.Entities
{
    public class CheckListItem : EntityBase
    {
        public bool IsChecked { get; set; }

        [MaxLength(256)]
        public string Text { get; set; }

        public virtual CheckList CheckList { get; set; }
        public int CheckListId { get; set; }
    }
}
