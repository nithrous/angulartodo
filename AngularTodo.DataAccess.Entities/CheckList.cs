﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AngularTodo.DataAccess.Entities
{
    public class CheckList : EntityBase
    {
        [MaxLength(256)]
        public string Title { get; set; }

        public int Color { get; set; }

        public virtual ICollection<CheckListItem> CheckListItems { get; set; } 
        public virtual ICollection<Image> Images { get; set; } 
    }
}
