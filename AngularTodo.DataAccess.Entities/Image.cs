﻿using System.ComponentModel.DataAnnotations;

namespace AngularTodo.DataAccess.Entities
{
    public class Image : EntityBase
    {
        [MaxLength(256)]
        public string RelativeLocation { get; set; }
        public int? CheckListId { get; set; }
    }
}
